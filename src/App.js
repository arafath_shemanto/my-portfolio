import { Route, Routes } from "react-router-dom";
import About from "./pages/About/About";
import Blog from "./pages/Blog/Blog";
import Contact from "./pages/Contact/Contact";
import Home from "./pages/Home/Home";
import Project from "./pages/Project/Project";
import Project_Details from "./pages/Project_Details/Project_Details";
import Footer from "./pages/Share/Footer";
import Header from "./pages/Share/Header";
function App() {
  return (
    <div className="App">
      <Header></Header>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="project" element={<Project />} />
        <Route path="contact" element={<Contact />} />
        <Route path="blog" element={<Blog />} />
        <Route path="about" element={<About />} />
        <Route path="project/:id" element={<Project_Details />} />
      </Routes>
      <Footer></Footer>
    </div>
  );
}

export default App;
