import React, { useRef } from "react";
import emailjs from "@emailjs/browser";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Contact = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_sc9lr6b",
        "template_r5ltikd",
        form.current,
        "-73QvZ-x4wHDOG3ul"
      )
      .then(
        (result) => {
          console.log(result.text);
          toast.success("successfully send .thankyou");

          e.target.reset();
        },
        (error) => {
          console.log(error.text);
          toast.error("Something went wrong");
        }
      );
  };
  return (
    <div>
      <div className="contact_area">
        <div className="container">
          <div className="row">
            <div className="col-xl-12">
              <div className="section_title_two text-center mb_60">
                <h3>Let's work together</h3>
                <p>Feel free to contact me anytime</p>
              </div>
            </div>
          </div>
          <div className="row justify-content-between">
            <div className="col-lg-6 col-xl-5">
              <div className="contact_info">
                <h3 className="theme_color font_30">Contact me</h3>
                <div className="address_list">
                  <p>
                    Phone: <span> +(88) 017 22 72 65 50</span>{" "}
                  </p>
                  <p>
                    Email: <span> arafathh.shemanto@gmail.com</span>
                  </p>
                  <p>
                    Location:
                    <span> 103/1, East Shalban, Rangpur, Bangladesh</span>{" "}
                  </p>
                </div>
                <div className="social_media_links2 ">
                  <ul className="justify-content-start">
                    <li>
                      <a href="#">
                        {" "}
                        <i className="fab fa-linkedin-in"></i>{" "}
                        <span>Linkedin</span>{" "}
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        {" "}
                        <i className="fab fa-twitter"></i> <span>twitter</span>{" "}
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        {" "}
                        <i className="fab fa-facebook-f"></i>{" "}
                        <span>facebook</span>{" "}
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        {" "}
                        <i className="fab fa-github"></i> <span>github</span>{" "}
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        {" "}
                        <i className="fab fa-instagram"></i>{" "}
                        <span>instagram</span>{" "}
                      </a>
                    </li>
                  </ul>
                </div>
                <div id="contact-map"></div>
              </div>
            </div>
            <div className="col-lg-6 col-xl-5">
              <div className="message_box">
                <h3 className="theme_color font_30 mb-40">Drop your message</h3>
                <form
                  className="form-area contact-form text-right"
                  ref={form}
                  onSubmit={sendEmail}
                >
                  <div className="row">
                    <div className="col-lg-12 form-group">
                      <input
                        type="text"
                        name="user_name"
                        placeholder="Name"
                        className="common-input mb_15 form-control"
                        required=""
                      />

                      <input
                        type="email"
                        name="user_email"
                        placeholder="Email"
                        className="common-input mb_15 form-control"
                        required=""
                      />
                    </div>
                    <div className="col-lg-12 form-group">
                      <textarea
                        className="common-textarea form-control mb_15"
                        name="message"
                        placeholder="Your message"
                        required=""
                      ></textarea>
                    </div>
                    <div className="col-lg-12">
                      <div className="alert-msg"></div>
                      {/* <button className="theme_button w-100">Send Message</button> */}
                      <input
                        className="theme_button w-100"
                        type="submit"
                        value="Send Message"
                      />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Contact;
