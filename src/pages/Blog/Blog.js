import React from "react";

import img1 from "../../asstes/img/blog/1.png";
import img2 from "../../asstes/img/blog/2.png";
import img3 from "../../asstes/img/blog/3.png";
import img4 from "../../asstes/img/blog/4.png";
const Blog = () => {
  return (
    <div class="blog_area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section_title d-flex align-items-center mb_60">
              <div class="verticalbar"></div>
              <h3>My Latest Blog</h3>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-10">
            <div class="single_blog">
              <div class="blog_thumb">
                <a href="blog_details.html">
                  <img src={img1} alt="" />
                </a>
              </div>
              <div class="blog_content">
                <a href="blog_details.html">
                  <h4>How to start your first design thinking project</h4>
                </a>
                <p>
                  Face years saying also after brought two isn't kind fifth. Air
                  heaven so hath multiply brought behold yielding. Together
                  behold great fruit she'd sixth give years.
                </p>
                <a href="blog_details.html" class="theme_button">
                  read more
                </a>
              </div>
            </div>
            <div class="single_blog">
              <div class="blog_thumb">
                <a href="blog_details.html">
                  <img src={img2} alt="" />
                </a>
              </div>
              <div class="blog_content">
                <a href="blog_details.html">
                  <h4>How to start your first design thinking project</h4>
                </a>
                <p>
                  Face years saying also after brought two isn't kind fifth. Air
                  heaven so hath multiply brought behold yielding. Together
                  behold great fruit she'd sixth give years.
                </p>
                <a href="blog_details.html" class="theme_button">
                  read more
                </a>
              </div>
            </div>
            <div class="single_blog">
              <div class="blog_thumb">
                <a href="blog_details.html">
                  <img src={img3} alt="" />
                </a>
              </div>
              <div class="blog_content">
                <a href="blog_details.html">
                  <h4>How to start your first design thinking project</h4>
                </a>
                <p>
                  Face years saying also after brought two isn't kind fifth. Air
                  heaven so hath multiply brought behold yielding. Together
                  behold great fruit she'd sixth give years.
                </p>
                <a href="blog_details.html" class="theme_button">
                  read more
                </a>
              </div>
            </div>
            <div class="single_blog">
              <div class="blog_thumb">
                <a href="blog_details.html">
                  <img src={img4} alt="" />
                </a>
              </div>
              <div class="blog_content">
                <a href="blog_details.html">
                  <h4>How to start your first design thinking project</h4>
                </a>
                <p>
                  Face years saying also after brought two isn't kind fifth. Air
                  heaven so hath multiply brought behold yielding. Together
                  behold great fruit she'd sixth give years.
                </p>
                <a href="blog_details.html" class="theme_button">
                  read more
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blog;
