import React from "react";

const Footer = () => {
  return (
    <footer>
      <div className="footer_wrapper">
        <div className="social_media_links">
          <ul>
            <li>
              <a href="#">
                {" "}
                <i className="fab fa-linkedin-in"></i> <span>Linkedin</span>{" "}
              </a>
            </li>
            <li>
              <a href="#">
                {" "}
                <i className="fab fa-twitter"></i> <span>twitter</span>{" "}
              </a>
            </li>
            <li>
              <a href="#">
                {" "}
                <i className="fab fa-facebook-f"></i> <span>facebook</span>{" "}
              </a>
            </li>
            <li>
              <a href="#">
                {" "}
                <i className="fab fa-github"></i> <span>github</span>{" "}
              </a>
            </li>
            <li>
              <a href="#">
                {" "}
                <i className="fab fa-instagram"></i> <span>instagram</span>{" "}
              </a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
