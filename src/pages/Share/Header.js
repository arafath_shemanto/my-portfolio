import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from "../../asstes/img/logo.png";
const Header = () => {
  return (
    <Navbar collapseOnSelect className="header_area" expand="lg" variant="dark">
      <Container fluid>
        <Link to="/">
          <img src={logo} alt="" />
        </Link>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse
          className="main_menu justify-content-end"
          id="responsive-navbar-nav "
        >
          <Nav className="align-items-center">
            <Link className="menu_item" to="/">
              home
            </Link>
            <Link className="menu_item" to="/about">
              about
            </Link>
            <Link className="menu_item" to="/project">
              projects
            </Link>
            <Link className="menu_item" to="/blog">
              blog
            </Link>
            <Link className="menu_item" to="/contact">
              contact
            </Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
