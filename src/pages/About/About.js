import React from "react";
import { Link } from "react-router-dom";
import profileThumb from "../../asstes/img/about/person.png";

const About = () => {
  return (
    <div class="bio_about_area">
      <div class="container">
        <div class="row">
          <div class="col-12 d-flex justify-content-between align-items-center mb_60">
            <div class="section_title d-flex align-items-center">
              <div class="verticalbar"></div>
              <h3>Resume</h3>
            </div>
            <div class="download_btn">
              <Link
                className="theme_button"
                to="arafath_frontend_developer.pdf"
                target="_blank"
                download
              >
                Download Resume
              </Link>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="resume_about mb_60">
              <div class="thumb">
                <img src={profileThumb} alt="" />
              </div>
              <div class="about_content">
                <h4 class="theme_color">About Me</h4>
                <p>
                  I'm Arafath Hossain, a Frontend web developer . I have more
                  than three years of experience. I have approved many themes
                  for ThemeForest and colorlib and web applications for
                  codecanyon
                </p>
                <div class="row">
                  <div class="col-md-10 col-lg-8">
                    <div class="about_info">
                      <div class="single_info">
                        <span>Full Name</span>
                        <p>: Arafath Hossain</p>
                      </div>
                      <div class="single_info">
                        <span>Experience</span>
                        <p>: 3 years +</p>
                      </div>
                      <div class="single_info">
                        <span>Nationality</span>
                        <p>: Bangladeshi</p>
                      </div>
                      <div class="single_info">
                        <span>Address:</span>
                        <p>: Rangpur, Bangladesh</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="resume_main_wrap">
          <div class="row">
            <div class="col-md-6">
              <div class="single_resume mb_60">
                <h4 class="theme_color mb_50 common_title f_w_600 ">
                  Experience
                </h4>
                <div class="resume_content mb_50">
                  <div class="round_dot"></div>
                  <h6 class="theme_color d-block font_14 f_w_500">
                    2019 - Present
                  </h6>
                  <h5>Frontend Developer</h5>
                  <span>at Spondonit</span>
                  <p>
                    Face years, saying. Also after brought two isn't kind fifth.
                    Air heaven so, hath multiply brought behold yielding life
                    god lesser third.
                  </p>
                </div>
                <div class="resume_content mb_50">
                  <div class="round_dot"></div>
                  <h6 class="theme_color d-block font_14 f_w_500">2018-2019</h6>
                  <h5>Frontend Developer</h5>
                  <span>at Envato</span>
                  <p>
                    Face years, saying. Also after brought two isn't kind fifth.
                    Air heaven so, hath multiply brought behold yielding life
                    god lesser third.
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="single_resume mb_60">
                <h4 class="theme_color mb_50 common_title f_w_600 ">
                  Education
                </h4>
                <div class="resume_content mb_50">
                  <div class="round_dot"></div>
                  <h6 class="theme_color d-block font_14 f_w_500">
                    2014 - 2018
                  </h6>
                  <h5>Diploma in Computer technology</h5>
                  <span>at Rangpur Polytechnic Institute</span>
                  <p>
                    Face years, saying. Also after brought two isn't kind fifth.
                    Air heaven so, hath multiply brought behold yielding life
                    god lesser third.
                  </p>
                </div>
                <div class="resume_content mb_50">
                  <div class="round_dot"></div>
                  <h6 class="theme_color d-block font_14 f_w_500">2012-2013</h6>
                  <h5>SSC</h5>
                  <span>at Shiksha Angon Heigh School, Rangpur</span>
                  <p>
                    Face years, saying. Also after brought two isn't kind fifth.
                    Air heaven so, hath multiply brought behold yielding life
                    god lesser third.
                  </p>
                </div>
              </div>
            </div>
            {/* <div class="col-md-6">
              <div class="single_resume mb_60">
                <h4 class="theme_color mb_50 common_title f_w_600 ">
                  Professional Skills
                </h4>
                <div class="resume_content mb_50">
                  <div class="single_skill">
                    <div class="skill_bar_title d-flex justify-content-between align-items-center">
                      <h6>User Research</h6>
                      <span class="font_14 f_w_400">75%</span>
                    </div>
                    <div class="single_bar">
                      <div class="progress">
                        <div
                          class="progress-bar"
                          role="progressbar"
                          style="width: 75%"
                          aria-valuenow="75"
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                    </div>
                  </div>
                  <div class="single_skill">
                    <div class="skill_bar_title d-flex justify-content-between align-items-center">
                      <h6>HTML</h6>
                      <span class="font_14 f_w_400">75%</span>
                    </div>
                    <div class="single_bar">
                      <div class="progress">
                        <div
                          class="progress-bar"
                          role="progressbar"
                          style="width: 75%"
                          aria-valuenow="75"
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                    </div>
                  </div>
                  <div class="single_skill">
                    <div class="skill_bar_title d-flex justify-content-between align-items-center">
                      <h6>User Experience Design</h6>
                      <span class="font_14 f_w_400">65%</span>
                    </div>
                    <div class="single_bar">
                      <div class="progress">
                        <div
                          class="progress-bar"
                          role="progressbar"
                          style="width: 65%"
                          aria-valuenow="65"
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                    </div>
                  </div>
                  <div class="single_skill">
                    <div class="skill_bar_title d-flex justify-content-between align-items-center">
                      <h6>CSS</h6>
                      <span class="font_14 f_w_400">75%</span>
                    </div>
                    <div class="single_bar">
                      <div class="progress">
                        <div
                          class="progress-bar"
                          role="progressbar"
                          style="width: 75%"
                          aria-valuenow="75"
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                    </div>
                  </div>
                  <div class="single_skill">
                    <div class="skill_bar_title d-flex justify-content-between align-items-center">
                      <h6>JQuery</h6>
                      <span class="font_14 f_w_400">85%</span>
                    </div>
                    <div class="single_bar">
                      <div class="progress">
                        <div
                          class="progress-bar"
                          role="progressbar"
                          style="width: 85%"
                          aria-valuenow="85"
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> */}
            <div class="col-md-6">
              <div class="single_resume mb_60">
                <h4 class="theme_color mb_50 common_title f_w_600 ">
                  Language
                </h4>
                <div class="resume_content mb_50">
                  <div class="single_laguage">
                    <h6>Bangla</h6>
                    <div class="lang_parcent">
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                    </div>
                  </div>
                  <div class="single_laguage">
                    <h6>English</h6>
                    <div class="lang_parcent">
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2"></div>
                      <div class="round_dot2 opacity_20"></div>
                      <div class="round_dot2 opacity_20"></div>
                      <div class="round_dot2 opacity_20"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
