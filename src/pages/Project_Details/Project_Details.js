import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const Project_Details = () => {
  const { id } = useParams();
  console.log(useParams());
  const [projectDetails, setProjectDetails] = useState({});
  useEffect(() => {}, []);
  fetch(`project.json`)
    .then((res) => res.json())
    .then((data) => console.log(data));
  return (
    <div class="portfolio_area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section_title d-flex align-items-center mb_60">
              <div class="verticalbar"></div>
              <h3>Project Details {id}</h3>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-10">
            <div class="single_portfolio">
              <div class="portfolio_thumb">
                <img src="img/portfolio/1.png" alt="" />
              </div>
              <div class="portfolio_content">
                <div class="portfolio_content_left">
                  <h2>Uber app</h2>
                  <p>
                    Face years saying also after brought two isn't kind fifth.{" "}
                    <br />
                    Air heaven so hath multiply brought behold yielding.
                  </p>
                </div>
                <div class="portfolio_content_right">
                  <a href="#" class="theme_button">
                    Live View
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Project_Details;
