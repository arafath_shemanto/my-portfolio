import React from "react";
import { useNavigate } from "react-router-dom";

const Project_single = ({ projectItem }) => {
  const navigate = useNavigate();
  const { id, name, img, description, liveLink } = projectItem;
  const detailsHandeler = () => {
    navigate(`/project/${id}`);
  };
  return (
    <div className="col-xl-4 col-lg-6 col-md-6">
      <div className="project_widget mb-4">
        <div className="thumb">
          <img className="img-fluid" src={img} alt="" />
        </div>
        <div className="project_content">
          <div className="project__content_left">
            <h3>{name}</h3>
            <p>{description}</p>
            <div className="d-flex align-items-center gap-2">
              <button
                onClick={detailsHandeler}
                className="theme_button2 small_btn"
              >
                details
              </button>
              <a
                target="_blank"
                href={liveLink}
                className="theme_button small_btn"
              >
                Live View
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Project_single;
