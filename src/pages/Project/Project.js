import React, { useEffect, useState } from "react";

import Project_single from "./Project_single";

const Project = () => {
  const [project, setProject] = useState([]);
  useEffect(() => {
    fetch("project.json")
      .then((res) => res.json())
      .then((data) => setProject(data));
  }, []);
  return (
    <div className="portfolio_area">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="section_title d-flex align-items-center mb_60">
              <div className="verticalbar"></div>
              <h3>My Portfolio</h3>
            </div>
          </div>
        </div>
        <div className="row ">
          {project.map((projectItem, index) => (
            <Project_single
              key={index}
              projectItem={projectItem}
            ></Project_single>
          ))}

          {/* <div className="col-lg-10">
            <div className="single_portfolio">
              <div className="portfolio_thumb">
                <img src={p_img1} alt="" />
              </div>
              <div className="portfolio_content">
                <div className="portfolio_content_left">
                  <h2>Uber app</h2>
                  <p>
                    Face years saying also after brought two isn't kind fifth.{" "}
                    <br />
                    Air heaven so hath multiply brought behold yielding.
                  </p>
                </div>
                <div className="portfolio_content_right">
                  <a href="#" className="theme_button">
                    Live View
                  </a>
                </div>
              </div>
            </div>
            <div className="single_portfolio">
              <div className="portfolio_thumb">
                <img src={p_img2} alt="" />
              </div>
              <div className="portfolio_content">
                <div className="portfolio_content_left">
                  <h2>Branding design</h2>
                  <p>
                    Face years saying also after brought two isn't kind fifth.{" "}
                    <br />
                    Air heaven so hath multiply brought behold yielding.
                  </p>
                </div>
                <div className="portfolio_content_right">
                  <a href="#" className="theme_button">
                    Live View
                  </a>
                </div>
              </div>
            </div>
            <div className="single_portfolio">
              <div className="portfolio_thumb">
                <img src={p_img3} alt="" />
              </div>
              <div className="portfolio_content">
                <div className="portfolio_content_left">
                  <h2>Product design</h2>
                  <p>
                    Face years saying also after brought two isn't kind fifth.{" "}
                    <br />
                    Air heaven so hath multiply brought behold yielding.
                  </p>
                </div>
                <div className="portfolio_content_right">
                  <a href="#" className="theme_button">
                    Live View
                  </a>
                </div>
              </div>
            </div>
            <div className="load_more text-center">
              <a href="#" className="theme_button2">
                LOAD MORE
              </a>
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default Project;
