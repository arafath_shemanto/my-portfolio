import React from "react";
import Project from "../Project/Project";
import Banner from "./Banner";

const Home = () => {
  return (
    <>
      <Banner></Banner>
      <Project></Project>
    </>
  );
};

export default Home;
