import React from "react";
import { Link } from "react-router-dom";

const Banner = () => {
  return (
    <div className="banner_area banner_bg_1 bio_section">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="banner_inner">
              <div className="banner_text">
                <h1>I'm Arafath Hossain</h1>
                <h3 className="cd-headline clip is-full-width">
                  <span>Frontend Developer</span>
                  {/* <span className="cd-words-wrapper">
                    <b className="is-visible">Web Designer</b>
                    <b>UI/UX Designer</b>
                    <b>Developer</b>
                  </span> */}
                </h3>
                <Link
                  className="theme_button"
                  to="arafath_frontend_developer.pdf"
                  target="_blank"
                  download
                >
                  Download Resume
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
